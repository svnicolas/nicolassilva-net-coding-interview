﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SecureFlight.Api.Models;
using SecureFlight.Api.Utils;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;

namespace SecureFlight.Api.Controllers;

[ApiController]
[Route("[controller]")]
public class FlightsController(
    IService<Flight> flightService,
    IService<Passenger> passangerService,
    IRepository<Passenger> passengerRepository,
    IRepository<Flight> flightRepository,
    IMapper mapper)
    : SecureFlightBaseController(mapper)
{
    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get()
    {
        var flights = await flightService.GetAllAsync();
        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<FlightDataTransferObject>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Get(
        FlightByOriginDestinationDataTransferObject flightByOriginDestinationDataTransferObject)
    {
        var flights = await flightService
            .FilterAsync(e => e.OriginAirport
                                  .Equals(flightByOriginDestinationDataTransferObject.OriginAirport,
                                      StringComparison.InvariantCultureIgnoreCase)
                              && e.DestinationAirport.Equals(
                                  flightByOriginDestinationDataTransferObject.DestinationAirport,
                                  StringComparison.InvariantCultureIgnoreCase));
        return MapResultToDataTransferObject<IReadOnlyList<Flight>, IReadOnlyList<FlightDataTransferObject>>(flights);
    }

    [HttpPut]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> Put([FromRoute] string code,
        FlightPassengerDataTransferObject flightPassengerDataTransferObject)
    {
        var passanger = await passengerRepository.GetByIdAsync(flightPassengerDataTransferObject.PassengerId);
        if (passanger is null)
        {
            return NotFound("Passenger not located");
        }

        var flight = await flightRepository.GetByIdAsync(flightPassengerDataTransferObject.FlightId);
        if (flight is null)
        {
            return NotFound("Flight not located");
        }

        flight.Passengers.Add(passanger);
        await flightRepository.SaveChangesAsync();
        return MapResultToDataTransferObject<Flight, FlightDataTransferObject>(flight);
    }
    
    [HttpPut]
    [ProducesResponseType(typeof(FlightDataTransferObject), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(ErrorResponseActionResult))]
    public async Task<IActionResult> PutRemove([FromRoute] string code,
        FlightPassengerDataTransferObject flightPassengerDataTransferObject)
    {
        var passanger = await passengerRepository.GetByIdAsync(flightPassengerDataTransferObject.PassengerId);
        if (passanger is null)
        {
            return NotFound("Passenger not located");
        }

        if (!passanger.Flights.Any())
        {
            await passengerRepository.Delete(passanger);
            await passengerRepository.SaveChangesAsync();
            //return MapResultToDataTransferObject<Flight, FlightDataTransferObject>(flight);
        }

        var flight = await flightRepository.GetByIdAsync(flightPassengerDataTransferObject.FlightId);
        if (flight is null)
        {
            return NotFound("Flight not located");
        }

        flight.Passengers.Add(passanger);
        await flightRepository.SaveChangesAsync();
        return MapResultToDataTransferObject<Flight, FlightDataTransferObject>(flight);
    }
}