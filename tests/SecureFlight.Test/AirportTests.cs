using System.Threading.Tasks;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Moq;
using SecureFlight.Api.Controllers;
using SecureFlight.Api.Models;
using SecureFlight.Core.Entities;
using SecureFlight.Core.Interfaces;
using SecureFlight.Infrastructure.Repositories;
using Xunit;

namespace SecureFlight.Test
{
    public class AirportTests
    {
        [Fact]
        public async Task Update_Succeeds()
        {
            //Arrange
            var testingContext = new SecureFlightDatabaseTestContext();
            testingContext.CreateDatabase();
            var repository = new BaseRepository<Airport>(testingContext);
            //var mockRepository = new Mock<IRepository<Airport>>();
            // mockRepository.Setup(e => e.Update(It.IsAny<Airport>()))
            //     .Callback<Airport>((entity) =>
            //     {
            //         var entry = testingContext.Entry(entity);
            //         entry.State = EntityState.Modified;
            //     });
            var serviceMock = new Mock<IService<Airport>>();
            var mapperMock = new Mock<IMapper>();
            var airport = new Airport
            {
                Country = "USA",
                Name = "Airport",
                City = "New York"
            };
            await testingContext.Airports.AddAsync(airport);
            await testingContext.SaveChangesAsync();
            //TODO: Add test code here
            //Act
            // var controller = new AirportsController(repository, serviceMock.Object)

            //Assert
            
            testingContext.DisposeDatabase();
        }
    }
}
